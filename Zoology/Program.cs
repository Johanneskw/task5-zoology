﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime;
using System.Threading;

namespace Zoology
{
    //Abstract superclass
    public abstract class Animal
    {
        //Setting properties for all animals
        public string Name { get; set; }
        public int NumberOfLegs { get; set; }
        public int Weigth { get; set; }
        public string Size { get; set; }
        //Overloaded constructor
        public Animal(string name, int numberOfLegs, string size, int weigth)
        {
            Name = name;
            NumberOfLegs = numberOfLegs;
            Size = size;
            Weigth = weigth;
        }
        public Animal(string name)
        {
            Name = name;
            NumberOfLegs = 0;
            Size = "unknown";
            Weigth = 0;
        }

        public virtual void Run()
        {
            Console.WriteLine($"The {this.GetType().Name} {Name} is running");
        }

        public abstract void LearnTrick(string trick);

        static void Main(string[] args)
        {
            Dog dog = new Dog("Shepard", 4, "Large", 30, true);
            WildCat wc = new WildCat("Lynx");
            PetCat pc = new PetCat("Garfield", 4, "Small", 5);
            Snake p3 = new Snake("Python", 0, "Large", 3);
            Parrot parr = new Parrot("Alfie", 2, "Small", 1);
            Fishy fish = new Fishy("Magikarp", 0, "Medium", 9);
            Animal[] animals = new Animal[] { dog, wc, pc, p3, parr, fish };
            //Iterating trough animals to test behaviours, and interfaces.
            foreach (Animal a in animals)
            {
                a.LearnTrick("sit");
            }
            dog.Limp();
            wc.Walk();
            p3.Crawl();
            DisplayWelcome(animals);
        }
        
        //Linq queries, with user input.
        public static IEnumerable<string> SearchWeigth(Animal[] animals, int var)
        {
            IEnumerable<string> res =
                from Animal in animals
                where Animal.Weigth < var
                orderby Animal.Weigth descending
                select $"{Animal.Name}, {Animal.Weigth}kg";

            return res;
        }
        public static IEnumerable<string> SearchNoLegs(Animal[] animals, int var)
        {
            IEnumerable<string> res =
                from Animal in animals
                where Animal.NumberOfLegs < var
                orderby Animal.NumberOfLegs descending
                select $"{Animal.Name}, {Animal.NumberOfLegs} legs";

            return res;
        }
        //Main program, displaying animals and taking commands and inputs for queries. 
        public static void DisplayWelcome(Animal[] animals)
        {
            string quit = " ";
            while (quit != "Q")
            {
                Console.WriteLine("\n************ WELCOME TO ZOOLOGY **************");
                Console.WriteLine("               Press Q to exit");
                Console.WriteLine("\nHere are our Animals: ");
                foreach (Animal a in animals)
                {
                    Console.WriteLine($"{a.Name} is a {a.Size} sized {a.GetType().Name}, with {a.NumberOfLegs} legs, weighing {a.Weigth}kg.");
                }
                Console.WriteLine("\nHere are your options:\n1.Search for animals below a preferable weigth\n2. Search for animals having less then 'n' legs");
                Console.WriteLine("Please enter the number of the option you want to choose: ");
                int n;
                string uis = Console.ReadLine();
                if (Int32.TryParse(uis, out n))
                {
                    if (n == 1)
                    {
                        Console.WriteLine("Please enter preferable weigth: ");
                        IEnumerable<string> rs = SearchWeigth(animals, Int32.Parse(Console.ReadLine()));
                        Console.WriteLine("\nResults: ");
                        foreach (string item in rs)
                        {
                            Console.WriteLine(item);
                        }
                    }
                    else if (n == 2)
                    {
                        Console.WriteLine("Please enter preferable number of legs: ");
                        IEnumerable<string> rs = SearchNoLegs(animals, Int32.Parse(Console.ReadLine()));
                        Console.WriteLine("\nResults: ");
                        foreach (string item in rs)
                        {
                            Console.WriteLine(item);
                        }
                    }
                } else
                {
                    quit = uis.ToUpper();
                }
            }
        }
    }

    public class Dog : Animal, ILimp
    {
        //Special propery only for dogs
        public bool IsGoodDog { get; set; }
        public Dog(string name, int numberOfLegs, string size,int weigth, bool isGoodDog) : base(name, numberOfLegs, size, weigth)
        {
            IsGoodDog = isGoodDog;
        }

        public Dog(string name) : base(name)
        {
            IsGoodDog = true;
        }

        public override void LearnTrick(string trick)
        {
            Console.WriteLine($"{Name} has learnt new trick: {trick}");
        }

        public void Limp()
        {
            Console.WriteLine($"{Name} the dog is limping away..");
        }
    }

    public class Snake : Animal, ICrawl
    {
        public Snake(string name, int numberOfLegs, string size, int weigth) : base(name, numberOfLegs, size, weigth) { }

        public Snake(string name) : base(name) { }

        public override void LearnTrick(string trick)
        {
            Console.WriteLine("Snakes cant do tricks.. ");
        }

        public void Crawl()
        {
            Console.WriteLine($"{Name} the snake is crawling");
        }
    }

    public class Cat : Animal, IWalk
    {
        public Cat(string name, int numberOfLegs, string size, int weigth) : base(name, numberOfLegs, size, weigth) { }

        public Cat(string name) : base(name) { }

        //Handles override in cat superclass, because no cats can do tricks. 
        public override void LearnTrick(string trick)
        {
            Console.WriteLine("Cats can't learn tricks...");
        }

        public void Walk()
        {
            Console.WriteLine($"{Name} the cat is walking.");
        }
    }


    public class WildCat : Cat
    {
        public WildCat(string name, int numberOfLegs, string size, int weigth) : base(name, numberOfLegs, size, weigth) { }

        public WildCat(string name) : base(name) { }
    }

    public class PetCat : Cat
    {
        public PetCat(string name, int numberOfLegs, string size, int weigth) : base(name, numberOfLegs, size, weigth) { }

        public PetCat(string name) : base(name) { }
    }

    public class Parrot : Animal
    {
        public Parrot(string name, int numberOfLegs, string size, int weigth) : base(name, numberOfLegs, size, weigth) { }

        public Parrot(string name) : base(name) { }

        public override void LearnTrick(string trick)
        {
            if (trick == "talk")
            {
                Console.WriteLine($"{Name} learned {trick}");
            }
            else { Console.WriteLine($"{Name} can't learn {trick}"); }
        }
    }

    public class Fishy : Animal
    {
        public Fishy(string name, int numberOfLegs, string size, int weigth) : base(name, numberOfLegs, size, weigth) { }
        public Fishy(string name) : base(name) { }
        public override void LearnTrick(string trick)
        {
            Console.WriteLine($"Fishy learnt a new trick: {trick}");
        }
    }

    public interface IWalk
    {
        public void Walk();
    }

    public interface ICrawl
    {
        public void Crawl();
    }

    public interface ILimp
    {
        public void Limp();
    }



}
